'''
Created on Jul. 3, 2019

@author: joey.gaudet
'''
from config import Config
import request_sender, json_builder, module_finder, logging
config_file = 'configuration.properties'
props = Config(config_file)
log_handler = props.create_logger()
log = logging.getLogger('config.' + __name__)

klass = module_finder.get_module(props.DBModuleName)
db_info = props.get_DB_info()
obj = klass(db_info)
more_data = True
runs = 0
cols = obj.get_cols()
while more_data == True:
    
    rows, more_data, send = obj.get_rows(more_data, runs)
    if send == True:
        
        post_request = json_builder.jsonBuilder(cols, rows)
        payload = post_request.build_request_json()
        request = request_sender.RequestSender(payload)
        r = request.send_request()
        runs += 1
