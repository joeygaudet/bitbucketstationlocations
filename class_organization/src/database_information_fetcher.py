'''
Created on Jul. 3, 2019

@author: joey.gaudet
'''
import psycopg2, logging, sys
log = logging.getLogger('config.' + __name__)
class Database_handler:
        
    def __init__(self, db_info):
        
        self.table_name = db_info['DBConfig.TableName']
        self.schema_name = db_info['DBConfig.SchemaName']
        self.fetch_count = int(db_info['DBConfig.FetchCount'])
        
        host = db_info['DBConfig.DBHost']
        user = db_info['DBConfig.DBUser']
        password = db_info['DBConfig.DBPassword']
        db_name = db_info['DBConfig.DBName']
        
        self.connection = psycopg2.connect(user = user, password = password, host = host, database = db_name)
        log.debug('Connection to database established')
        self.cols_cursor = self.connection.cursor('cols')
        log.debug('"cols" server side cursor created')
        self.cursor = self.connection.cursor('cursor')
        log.debug('"cursor" server side cursor created')
        return self.table_name, self.schema_name
    
    
    def get_rows(self, more_data, runs, send = True):
        
        if runs == 0:
            query = '''SELECT * FROM ''' + self.table_name + ''' WHERE id < 29500'''
            log.info('Executing query "%s"',query)
            try:
                self.cursor.execute(query)
                log.info('No errors in query execution')
            except:
                log.critical('Error in query syntax, cannot fetch any data, exiting...')
                self.close_connections([self.cursor, self.connection])
                sys.exit()
        log.info('Fetching rows...')
        rows = self.cursor.fetchmany(self.fetch_count)
        if len(rows) < self.fetch_count:
            more_data = False
            self.close_connections([self.connection, self.cursor])
            log.info('Done fetching data')
            if rows == []:
                log.info('Not sending dataset, empty payload')
                send = False
        return rows, more_data, send
    def close_connections(self, *connections):
        to_close = connections[0]
        for connection in to_close:
            if connection:
                connection.close()
                log.info('%s closed', connection)
    def get_cols(self): #https://dataedo.com/kb/databases/all/information_schema
        pass
