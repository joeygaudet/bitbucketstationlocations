'''
Created on Jul. 10, 2019

@author: joey.gaudet
'''
import logging
log = logging.getLogger('config.' + __name__)
def get_module(path):
    from importlib import import_module
    module_path, _, class_name = path.rpartition('.')
    mod = import_module(module_path)
    klass = getattr(mod, class_name)
    log.debug('Imported and returning %s',klass)
    return klass
