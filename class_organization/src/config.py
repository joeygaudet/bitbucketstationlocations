'''
Created on Jul. 4, 2019

@author: joey.gaudet
'''
from jproperties import Properties
import sys, logging, datetime
from logging import handlers
class Config:
    def __init__(self,config_file):
        self.props = Properties()
        f = open(config_file,'rb')
        self.props.load(f, encoding='utf-8')
        self.db_info = {}
        for key in self.props:
            setattr(self,key,self.props[key][0])
            if key.startswith('DBConfig'):
                self.db_info.update({key:self.props[key][0]})
    def create_logger(self):
        logger = logging.getLogger(__name__)
        logger.setLevel(logging.DEBUG)
        file_handle = handlers.RotatingFileHandler(self.LogFileName, maxBytes=int(self.MaxBytes), backupCount=int(self.BackupCount))
        file_handle.setLevel(self.LogFileLevel)
        console_stream = logging.StreamHandler(sys.stdout)
        console_stream.setLevel(self.LogConsoleLevel)
        logger.addHandler(file_handle)
        logger.addHandler(console_stream)
        logger.debug('Successfully created logger at %s',datetime.datetime.now())
        return logger
    
    def get_DB_info(self):
        return self.db_info
