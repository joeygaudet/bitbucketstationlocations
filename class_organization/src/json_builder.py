'''
Created on Jul. 3, 2019

@author: joey.gaudet
'''
from _datetime import datetime
import logging
log = logging.getLogger('config.' + __name__)
class jsonBuilder:
    
    def __init__(self, cols, rows):
        self.cols = cols
        self.rows = rows
        
    def build_request_json(self):
        
        data = {}
        for line_number in self.rows:
            new_data, timestamp = jsonBuilder._build_row_json(self, line_number)
            data.update({timestamp:new_data})
            log.debug('Added %s:%s to json data, send pending',timestamp,new_data)
        self.data = data
        return data
    
    def _build_row_json(self, row): #_ indicates private method (should only be called within class)
        
        dic = {}
        count = 0
        for line_number in row:
            if type(line_number) is datetime:
                line_number = str(line_number)
            dic.update({self.cols[count]:line_number})
            count += 1
        timestamp = str(dic['messagetime'])
        return dic, timestamp