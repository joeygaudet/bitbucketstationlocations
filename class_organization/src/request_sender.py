'''
Created on Jul. 3, 2019

@author: joey.gaudet
'''
import json, requests, logging
log = logging.getLogger('config.' + __name__)

class RequestSender:
    def __init__(self, data, url = 'http://www.postman-echo.com/post'):
        self.data = data
        self.url = url
    def send_request(self):
        log.info('Sending %s to %s', self.data, self.url)
        r = requests.post(self.url, data = self.data)
        if r.status_code == 200:
            log.info('Request successful')
        else:
            log.error('Request not sent, status code: %s', r.status_code)
        return r