'''
Created on Jul. 5, 2019

@author: joey.gaudet
'''
from database_information_fetcher import Database_handler
import logging, sys
log = logging.getLogger('config.' + __name__)
class Postgres_handler(Database_handler):
    
    def __init__(self,db_info):
        self.table_name, self.schema_name = super().__init__(db_info)
        
    def get_cols(self):  #works with postgres and some other modern databases, will have to be reviewed if not using postgres
        query = "SELECT * FROM information_schema.columns WHERE table_schema = '" + self.schema_name + "' AND table_name = '" + self.table_name + "'"
        try:
            self.cols_cursor.execute(query)
            information_rows = self.cols_cursor.fetchall()
        except:
            log.critical('Could not determine column names, error in query. Exiting...')
            super().close_connections([self.cols_cursor, self.cursor, self.connection])            
            sys.exit()
        self.cols_cursor.close()
        cols = []
        for line_number in information_rows:
            cols.append(line_number[3])
        log.debug('Detected database columns %s', cols)
        return cols