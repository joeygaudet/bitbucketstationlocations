'''
Created on Jun. 18, 2019

@author: joey.gaudet
'''
import json
from psycopg2._psycopg import Date, Time

def get_value(config_file, value, subvalue = None):
    ''' 
    Returns the requested value from .json config file.
    value must be a key in json dictionary
    '''
    f = open(config_file, 'r')
    config = json.load(f)
    if value == 'Columns':
        return_val = []
        for i in config['Columns']:
            return_val.append(i)
    if subvalue is not None:
        return_val = config[value][subvalue]
    else:
        return_val = config[value]
    f.close()
    return return_val

def value_cleaner(value, i, config_file, columns):
    BOOLS = {'Yes':'True','No':'False'}
    value = value.replace("'",'')
    f = open(config_file, 'r')
    config = json.load(f)
    if config['Columns'].get(columns[i]) == 'boolean':
        value = BOOLS[value]
    elif config['Columns'].get(columns[i]) == 'date':
        m,d,y = value.split('/')
        value = str(Date(int(y),int(m),int(d)))
    elif config['Columns'].get(columns[i]) == 'time':
        h,m,s = value.split(':')
        value = str(Time(int(h),int(m),int(s)))
    elif value == '':
        value = 'null'
    elif config['Columns'].get(columns[i]) == 'text':
        value = "'" + value + "'"
    f.close()
    return value

'''
    if value == 'No':
        value = 'False'
    elif value == 'Yes':
            value = 'True'
    if i == 0:
            m,d,y = value.split('/')
            value = y + '-' + m + '-' + d
    if value == '':
        value = 'null'
    value = value.replace("'",'')
    if 0 <= i <= 5 or i == 18 or i == 19 or 21 <= i <= 28 or 29 <= i <= 34:
        value = "'" + value + "'"
    return value
    '''