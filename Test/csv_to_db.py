'''
Created on May 30, 2019

@author: joey.gaudet
'''
import psycopg2, csv, logging
COLS = ['seqID','DateOfStop','TimeOfStop','Agency','SubAgency','Description','Location','Latitude','Longitude','Accident','Belts','PersonalInjury','PropertyDamage','Fatal','CommercialLicense','HAZMAT','CommercialVehicle','Alcohol','WorkZone','State','VehicleType','Year','Make','Model','Color','ViolationType','Charge','Article','ContributedToAccident','Race','Gender','DriverCity','DriverState','DLState','ArrestType','Geolocation']
logging.basicConfig(level = logging.DEBUG, filename = 'logging.txt', filemode = 'a')
connection = psycopg2.connect(user = "jasco" ,password = "jasco123" ,host = "142.176.15.238", database = "JoeyTest")
cursor = connection.cursor()
names = ""
count = 0
for i in range(len(COLS)):
    if i < len(COLS) - 1:
        names += COLS[i] + ","
    else:
        names += COLS[i]
with open('Traffic_Violations.csv') as csv_file:
    rows = csv.reader(csv_file, delimiter=',')
    for item in rows:
        SQL_query = "INSERT INTO mobile (" + names +") VALUES ("
        for i in range(len(item)):
            if item[i] == 'No':
                item[i] = 'False'
            elif item[i] == 'Yes':
                item[i] = 'True'
            if i == 1:
                m,d,y = item[i].split('/')
                item[i] = y + '-' + m + '-' + d
            if item[i] == '':
                item[i] = 'null'
            item[i] = item[i].replace("'",'')
            if 0 <= i <= 6 or i == 19 or i == 20 or 22 <= i <= 27 or 29 <= i <= 34:
                item[i] = "'" + item[i] + "'"
            if i != len(item) - 1:
                SQL_query += item[i] + ","
            else:
                SQL_query += item[i] + ');'
       
        cursor.execute(SQL_query)
        connection.commit()
        count+= 1
        if count%100 == 0:
            print(count)
        logging.debug('Row %d was successfully inserted', count)
csv_file.close()

if (connection):
    cursor.close()
    connection.close()
    print("PostgreSQL connection is closed")
    