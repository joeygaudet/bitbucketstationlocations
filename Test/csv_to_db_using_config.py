'''
Created on May 30, 2019

@author: joey.gaudet
'''
import psycopg2, csv, logging
from config_funcs import get_value, value_cleaner
config = 'config.json'
COLS = get_value(config, 'Columns')
logging.basicConfig(level = get_value(config,'LoggingConfig','Level'), filename = get_value(config,'LoggingConfig','FileName'), filemode = get_value(config,'LoggingConfig','FileMode'))
connection = psycopg2.connect(user = get_value(config,'DBConfig','User') ,password = get_value(config,'DBConfig','Password') ,host = get_value(config,'DBConfig','Host'), dbname = get_value(config,'DBConfig','Database'))
cursor = connection.cursor()
names = ""
count = 0
COLS_LIST = []
for i in COLS.keys():
    COLS_LIST.append(i)
    if len(COLS_LIST) < len(COLS):
        names += i + ","
    else:
        names += i
with open(get_value(config,'CSVName')) as csv_file:
    rows = csv.reader(csv_file, delimiter=',')
    for item in rows:
        SQL_query = "INSERT INTO traffic_violations (" + names +") VALUES ("
        for i in range(len(item)):
            clean_value = value_cleaner(item[i], i, config, COLS_LIST)
            if i != len(item) - 1:
                SQL_query += clean_value + ","
            else:
                SQL_query += clean_value + ');'
        cursor.execute(SQL_query)
        count+= 1
        if count%100 == 0:
            connection.commit()
            print(count)
            logging.debug('Rows %d to %d transferred to database', count - 100, count - 1)
        logging.debug('Row %d added to pending transactions', count)
csv_file.close()
if (connection):
    cursor.close()
    connection.close()
    print("PostgreSQL connection is closed")
    