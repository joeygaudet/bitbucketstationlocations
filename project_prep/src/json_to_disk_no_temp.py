'''
Created on Jun. 17, 2019

@author: joey.gaudet
'''
import json, os
from zipfile import ZipFile
read_file = open('cards\\collectable_cards.json','r')
json_list = list(read_file)
zip = ZipFile('sorted_cards.zip', 'w')
for line_number in json_list:
    line_number.rstrip()
    card = json.loads(line_number)
    f = open('card','w')
    print(card, file = f)
    f.flush()
    f.close()
    zip.write('C:\\Users\\joey.gaudet\\eclipse-workspace\\web_requests\\src\\card', arcname = card['cardSet'] + '\\' + card['name'] + '.json')
    
os.remove('C:\\Users\\joey.gaudet\\eclipse-workspace\\web_requests\\src\\card')
