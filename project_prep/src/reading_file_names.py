'''
Created on Jul. 9, 2019

@author: joey.gaudet
'''

from dateutil import parser
import os
files = os.listdir('random_files')
for num, i in enumerate(files):
    
    file_name,_,file_type = i.rpartition('.')
    try:
        date = parser.parse(file_name)
    except:
        print('Invalid date format')
        break
    print('File ',num+1)
    print('---------')
    print('Date: ',date.date())
    print('Time: ',date.time())
    print('File Type: ',file_type + '\n')
else:
    print('All files in directory recorded')