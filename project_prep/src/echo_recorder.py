'''
Created on Jun. 24, 2019

@author: joey.gaudet
'''
import requests, datetime
f = open('echo_config.txt','r')
words = f.readline()
words_list = words.split(',')
f.close()
path = 'C:\\Users\\joey.gaudet\\eclipse-workspace\\web_requests\\src\\responses\\'
url = 'https://postman-echo.com/get'
for line_number in words_list:
    dt = datetime.datetime.now()
    time = str(dt.year) + str(dt.month) + str(dt.day) + 'T' + str(dt.hour) + str(dt.minute) + str(dt.second)
    f = open(path + time + line_number +'.txt', 'w')
    headers = {'Coolword':line_number}
    response = requests.get(url,headers = headers)
    print(response.text, file = f)
    f.close()