'''
Created on Jun. 24, 2019

@author: joey.gaudet
'''
import datetime, subprocess
f = open('echo_config.txt','r')
words = f.readline()
words_list = words.split(',')
f.close()
path = 'C:\\Users\\joey.gaudet\\eclipse-workspace\\project_prep\\src\\responses\\'
url = 'http://postman-echo.com/get'
for line_number in words_list:
    dt = datetime.datetime.now()
    time = str(dt.year) + str(dt.month) + str(dt.day) + 'T' + str(dt.hour) + str(dt.minute) + str(dt.second)
    f = open(path + time + line_number +'.txt', 'w')
    headers = '"coolword: ' + line_number +'" '
    subprocess.run(('curl -H ' + headers + url), shell = True, stdout = f, env = 'PATH')
    f.close()