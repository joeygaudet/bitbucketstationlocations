'''
Created on Jul. 12, 2019

@author: joey.gaudet
'''

from jinja2 import Environment, FileSystemLoader
file_loader = FileSystemLoader('templates')
env = Environment(loader = file_loader)

def hello_world():
    temp1 = env.get_template('hello_world.txt')
    output = temp1.render()
    print(output)

def animal_owner(dic):
    temp2 = env.get_template('animals.txt')
    output = temp2.render(dic)
    print(output)

def determine_equality(x,y):
    temp3 = env.get_template('truth.txt')
    truth = False
    if x == y:
        truth = True
    output = temp3.render(truth = truth)
    print(output)
    
def print_list(lst):
    temp4 = env.get_template('print_list.txt')
    output = temp4.render(lst = lst)
    print(output)
hello_world()
animal_owner({'name':'Joe','animal':'Dog'})
determine_equality(0,0)
determine_equality(0,1)
print_list([1,2,3])