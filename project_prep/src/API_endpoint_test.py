'''
Created on Jun. 19, 2019

@author: joey.gaudet
'''
import requests
url = 'https://postman-echo.com/post'
payload = 'foo1=bar1&foo2=bar2'
headers = {'Content-Type':'application/json'}
response = requests.post(url, headers = headers, data = payload, allow_redirects=False)
print(response.text)