'''
Created on Jun. 11, 2019

@author: joey.gaudet
'''

import json, tempfile, os

from zipfile import ZipFile
TEMP_FILES = []
read_file = open('cards\\collectable_cards.json','r')
json_list = list(read_file)
zip = ZipFile('C:\\Users\\joey.gaudet\\Desktop\\sorted_cards.zip', 'w')
for line_number in json_list:
    line_number.rstrip()
    card = json.loads(line_number)
    temp_json = tempfile.NamedTemporaryFile(mode = 'w', delete = False)
    print(card, file = temp_json.file)
    TEMP_FILES.append(temp_json.name)
    print(temp_json.name)
    temp_json.file.flush()
    temp_json.file.close()
    zip.write(temp_json.name, arcname = card['cardSet'] + '\\' + card['name'] + '.json')
    
for stop_index in TEMP_FILES:
    os.remove(stop_index)
