'''
Created on Jul. 16, 2019

@author: joey.gaudet
'''
from PIL import Image
im = Image.open('deer.gif')
def displayimage(im):
    im.show()
def cropimage(im,box):
    return im.crop(box)

box = (100,100,400,400)
region = im.crop(box)
region = region.transpose(Image.ROTATE_180)
im.paste(region,box)
im.show()
