'''
Created on Jun. 7, 2019

@author: joey.gaudet
'''
import requests, psycopg2
COLS = ['cardId','dbfId','name','cardSet','type','faction','rarity','cost','attack','health','text','flavor','artist','collectible','elite','playerClass','img','imgGold','Taunt','mechanics']
STRINGIFY = ['text','cardId','name','cardSet','type','playerClass','img','imgGold','faction','rarity','flavor','artist']
names = ''
for column in range(len(COLS)):
    if column < len(COLS) - 1:
        names += COLS[column] + ","
    else:
        names += COLS[column]
headers =  {"X-RapidAPI-Key":"ed9500cf53msh3a27034d2d6db34p1a0d03jsnb0b853af6d37","X-RapidAPI-Host":"omgvamp-hearthstone-v1.p.rapidapi.com"}
PARAMS = {'collectible':1}
connection = psycopg2.connect(user = "jasco" ,password = "jasco123" ,host = "142.176.15.238", database = "JoeyTest")
cursor = connection.cursor()
r = requests.get('https://omgvamp-hearthstone-v1.p.rapidapi.com/cards',PARAMS,headers = headers)
all_cards = r.json()
count = 0
keys = []
for k in all_cards.keys():
    keys.append(k)
for line_number in range(len(keys)):
    set = all_cards[keys[line_number]]
    for card in set:
        mechanics = []
        count = 0
        SQL_query1 = ""
        SQL_query2 = ""
        for column in card:
            if column in COLS:
                value = card.get(column)
                if column == 'mechanics':
                    dict = value[0]
                
                    for line_number in dict:
                        val = dict.get(line_number)
                        val = '"' + val + '"'
                        mechanics.append(val)
                    value = 'ARRAY' + str(mechanics) + '::text[]'
                if column in STRINGIFY and "'" in value:
                    value = value.replace("'","''")
                    value = "'" + value + "'"
                elif column in STRINGIFY:
                    value = "'" + value + "'"
                if count != 0:
                    SQL_query1 += "," + column
                    SQL_query2 += "," + str(value)
            
                else:
                    SQL_query1 += column
                    SQL_query2 += value

            count+=1
        SQL_query = '''INSERT INTO collectable_cards(''' + SQL_query1 + ''') VALUES ('''+ SQL_query2 + ''');'''
        print(SQL_query)
        cursor.execute(SQL_query)
        connection.commit()


if (connection):
    cursor.close()
    connection.close()
    print("PostgreSQL connection is closed")
