# coding=<UTF-8>
'''
Created on Jun. 10, 2019

@author: joey.gaudet
'''

import logging, sys
from zipfile import ZipFile
log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
console_log = logging.StreamHandler(sys.stdout)
console_log.setLevel(logging.INFO)
file_stream = logging.FileHandler('information.log')
file_stream.setLevel(logging.DEBUG)
log.addHandler(console_log)
log.addHandler(file_stream)
print(log.handlers)

f = ZipFile('cards.zip', 'w')
for line_number in range(5):
        card = 'C:\\Users\\joey.gaudet\\Desktop\\cards\\' + str(line_number) + '.json'
        f.write(card, arcname = str(line_number) + '.json')
        log.debug('File %s added to zip', card)
        if line_number == 4:
            log.info('All files inserted')
